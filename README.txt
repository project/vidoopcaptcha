
VidoopCAPTCHA module for Drupal
====================

The VidoopCAPTCHA module uses the VidoopSecure service to stop
spammers without burdening your users. For more information
and to sign-up for an account go to:

    http://login.vidoop.com

INSTALLATION
------------

0. VidoopCAPTCHA depends on the CAPTCHA module, you will
   need to install it before hand.  For more information about
   the Drupal CAPTCHA system see:
       http://drupal.org/project/captcha

1. Extract the VidoopCAPTCHA module to your
   modules directory (sites/all/modules).
   

CONFIGURATION
-------------
   
1. Enable VidoopCAPTCHA and CAPTCHA modules in:
       admin/build/modules
   
2. You'll now find a VidoopCAPTCHA tab in the CAPTCHA
   administration page available at:
       admin/user/captcha/vidoopcaptcha

6. Register for a VidoopSecure account:
       http://login.vidoop.com

7. Input your Customer ID, Site ID, plus your API username and
   password into the VidoopCAPTCHA settings. The rest of
   the settings should be fine as their defaults.

8. Visit the Captcha administration page and set where you
   want the VidoopCAPTCHA form to be presented:
       admin/user/captcha


CHANGELOG
---------


THANKS
---------
Huge thanks to the writers of the reCAPTCHA module that 
served as the base for the VioopSecure module.
