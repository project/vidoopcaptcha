jQuery(function() {
    var $ = jQuery;

    // Add placeholder text to an input box
    function add_placeholder(input, placeholder) {
        $(input).attr('placeholder', placeholder);
        if (!$.browser.safari) {
            $(input).each(function() {
                    if(this.value == '' || this.value == this.getAttribute("placeholder")){ // set placeholder style on inital page load
                      this.value = this.getAttribute("placeholder");
                      this.style.color = "#aaa"; 
                    }
                }).focus(function() { // reset style on focus
                    if (this.value == this.getAttribute("placeholder")) {
                        this.value = "";
                        try {
                            this.style.color = 'inherit';
                        } catch(e) {
                            this.style.color = '#000';
                        }
                    }
                }).blur(function() { // set placeholder style on blur
                    if (this.value.length < 1) {
                        this.value = this.getAttribute("placeholder");
                        this.style.color = "#aaa";
                    } 
                });

            $(input.form).submit(function() {
                $(this).find('input[placeholder]').each(function(){
                    if(this.value == this.getAttribute('placeholder')){
                        this.value = ''; // reset value if placeholder is still set as value
                    }
                });
            });
        }
    }

    // Constructor for a lightbox-style captcha object.
    function Captcha($input, captcha_id, instructions) {
        this.small = { width: 180, height: 1 };

        // Anchor the slideout to the node that was passed in
        this.anchor = $("<div style='position: relative; clear: both;'></div>").insertAfter($input);

        // This is the widget that slides out.
        this.$slider = $('<div class="vs_bubble"><p>' + get_instructions() + '</p><span><a href="#">hide &times;</a></span>' +
            '<img src="https://api.vidoop.com/vs/captchas/' + captcha_id + '/image"></div>')
            .appendTo(this.anchor)
            .css({ opacity: 0 });

        var me = this;
        this.input = $input[0];

        $(".vidoop_secure .vs_bubble").mousedown(function(e) {
            e.preventDefault();
        });

        $(".vidoop_secure .vs_bubble a").click(function(e) {
            $input[0].blur();
            e.preventDefault();
        });

        $input.focus(function(e) {
            me.slide_in(e);
        }).blur(function(e) {
            me.slide_out(e);
        });

        $(".vidoop_secure .vs_bubble img").load(function() {
            me.$slider.css('display', 'block');
            me.large = { width: me.$slider.width(), height: me.$slider.height() };
            me.$slider.css({
                'display': 'none',
                'width': me.small.width,
                'height': me.small.height
            });

            // Ensures that the page is long enough when captcha slides out.
            // Otherwise tabbing to the next field slides the page up.
            $('<div style="visibility:hidden;position:absolute;height:1px;">x</div>')
                .appendTo(me.anchor)
                .css('top', me.large.height);

            if (me.focused) {
                me.slide_in();
            }
        });
    }

    Captcha.prototype = {
        // show the captcha
        slide_in: function(e) {
            var me = this;

            if (this.large) {
                this.$slider.stop().animate({
                    'width': this.large.width,
                    'height': this.large.height,
                    'opacity': 1
                }, function() {
                    me.$slider[0].scrollIntoView();
                });
            }

            this.input.focus();
            this.focused = true;
        },

        // hide the captcha
        slide_out: function(e) {
            var me = this;

            this.$slider.stop().animate({
                width: this.small.width,
                height: this.small.height,
                opacity: '0'
            }, function() {
                me.$slider.hide();
            });

            this.focused = false;
        },

        // toggle whether captcha is shown or hidden
        slide_toggle: function(e) {
            if (this.$slider.css('display') == "none") {
                this.slide_in(e);
            }
            else {
                this.slide_out(e);
            }
        }
    };

    // adds CSS styling to the page
    function add_style() {
        $(['<style type="text/css">',
           '.vidoop_secure { position: relative }',
           '.vidoop_secure span { position: absolute; right: 10px; top: 10px; }',
           '.vidoop_secure .vs_bubble { text-align: center; padding: 5px; position: absolute; z-index: 999; display: none; background-color: #eee; border: solid 1px #444; margin-bottom: 4px; }',
           '.vidoop_secure p { font-size: 12px; color: black; width: 100%; }',
           '</style>'
        ].join("\n")).prependTo("head");
    }

    // Create instructions for solving the image shield.
    function get_instructions() {
        return vidoop_secure.instructions;
    }

    add_style();

    // vidoop_secure node contains several important things:
    // * a hidden input field with the captcha ID as its value
    // * a text box that determines when captcha is shown
    // * an image that the user can click to see the captcha
    // * a comment node with the categories to choose
    var $input = $('.vidoop_secure input:text');
    add_placeholder($input[0], "Click here to fill me in");
    var captcha = new Captcha($input, $('.vidoop_secure input:hidden').val(), get_instructions());
    $('.vidoop_secure .vs_button').click(function(e) {
        captcha.slide_toggle();
    });
});
