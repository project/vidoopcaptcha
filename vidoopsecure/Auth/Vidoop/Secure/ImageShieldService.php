<?php

require_once dirname(__FILE__) . '/Service.php';

/**
 * VidoopSecure ImageShield Service.
 */
class Auth_Vidoop_Secure_ImageShieldService extends Auth_Vidoop_Secure_Service {

	/**
	 * Request a new image sheild be generated.
	 *
	 * @param array $args options for generated ImageShield.  Valid array keys are:
	 * 		imageshield_length => Number of categories that must be entered to solve the ImageShield.
	 * 		order_matters      => Whether the categories should be entered in order.
	 * 		width              => Width of the ImageShield in images.
	 * 		height             => Height of the ImageShield in images.
	 * 		image_code_color   => Color of the image code.
	 * 		image_code_length  => Number of characters in each image code.
	 * 		bundle             => Array of category-ids. It must be height * width elements in length. 
	 * 		                      The first imageshield_length category-ids are used as the auth-categories.
	 *
	 * @return Auth_Vidoop_ImageShield object
	 */
	public function new_imageshield($args = null) {
		$api_url = $this->api_base . '/vs/customers/' . $this->site->customer . '/sites/' . $this->site->name . '/services/imageshield';

		$credentials = $this->site->username . ':' . $this->site->password;
		$args['bundle'] = join(':', $args['bundle']);
		$response = $this->vs->api_call($api_url, 'POST', $args, $credentials);

		if ($response['status'] == 201) {
			return Auth_Vidoop_ImageShield::from_xml($response['body']);
		}
	}


	/**
	 * Get the requested ImageShield.
	 *
	 * @param string ID of the ImageShield
	 * @return Auth_Vidoop_ImageShield object
	 */
	public function get_imageshield($id) {
		$api_url = $this->api_base . '/vs/imageshields/' . $id;

		$response = $this->vs->api_call($api_url, 'GET');

		if ($response['status'] == 200) {
			return Auth_Vidoop_ImageShield::from_xml($response['body']);
		}
	}


	/**
	 * Verify the code for a ImageShield.
	 *
	 * @param string $id ID of ImageShield
	 * @param string $code code to test for ImageShield
	 *
	 * @return boolean true if the code is valid for the ImageShield
	 */
	public function verify_code($id, $code) {
		$api_url = $this->api_base . '/vs/imageshields/' . $id;

		$response = $this->vs->api_call($api_url, 'POST', array('code' => $code));

		if ($response['status'] == 200) {
			return true;
		} else {
			return false;
		}
	}

}


/**
 * ImageShield provided by the VidoopSecure ImageShield service.
 */
class Auth_Vidoop_ImageShield {

	/** 
	 * ImageShield ID
	 *
	 * @var string
	 */
	public $id;

	/**
	 * Number of categories that must be entered to solve the ImageShield.
	 *
	 * @var int
	 */
	public $imageshield_length;

	/**
	 * Whether the categories must be entered in order.
	 *
	 * @var boolean
	 */
	public $order_matters;

	/**
	 * Width of the ImageShield in number of images.
	 *
	 * @var int
	 */
	public $width;

	/**
	 * Height of the ImageShield in number of images.
	 *
	 * @var int
	 */
	public $height;

	/**
	 * Color of the image code.
	 *
	 * @var string
	 */
	public $image_code_color;

	/**
	 * Number of characters in each image code.
	 */
	public $image_code_length;

	/**
	 * URL of ImageShield image to display to the user.
	 *
	 * @var string
	 */
	public $image;

	/**
	 * Has identification of this ImageShield already been attempted.
	 *
	 * @var boolean
	 */
	public $attempted;

	/**
	 * Was this ImageShield successfully authenticated.
	 *
	 * @var boolean
	 */
	public $authenticated;

	/**
	 * Raw XML string this ImageShield object was created from.
	 *
	 * @var string
	 */
	public $xml;

	/**
	 * Generate an ImageShield instance from the provided XML.
	 *
	 * @param string $xml XML provided from VidoopSecure API
	 * @return Auth_Vidoop_ImageShield ImageShield instance
	 */
	public static function from_xml($xml) {

		$simplexml = simplexml_load_string($xml);

		$shield = new Auth_Vidoop_ImageShield();
		$shield->xml = $xml;
		$shield->id = (string) $simplexml->id;
		$shield->imageshield_length = (int) $simplexml->imageshield_length;
		$shield->order_matters = Auth_Vidoop_Secure::parse_boolean($simplexml->order_matters);
		$shield->width = (int) $simplexml->width;
		$shield->height = (int) $simplexml->height;
		$shield->image_code_color = (string) $simplexml->image_code_color;
		$shield->image_code_length = (int) $simplexml->image_code_length;
		$shield->image = (string) $simplexml->imageURI;
		$shield->attempted = Auth_Vidoop_Secure::parse_boolean($simplexml->attempted);
		$shield->authenticated = Auth_Vidoop_Secure::parse_boolean($simplexml->authenticated);

		return $shield;
	}

}

?>
