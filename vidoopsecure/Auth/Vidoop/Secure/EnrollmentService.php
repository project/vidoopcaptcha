<?php

require_once dirname(__FILE__) . '/Service.php';

/**
 * VidoopSecure Enrollment Service.
 */
class Auth_Vidoop_Secure_EnrollmentService extends Auth_Vidoop_Secure_Service {

	/**
	 * Get an ImageShield category, or all available categories.
	 *
	 * @param string $id ID of category to retrieve.  If not specified, all categroies will be retreived.
	 * @return mixed Auth_Vidoop_Category or an array of Auth_Vidoop_Category objects.
	 */
	public function get_category($id = null) {
		$api_url = $this->api_base . '/vs/categories';
		if (!empty($id)) $api_url .= '/' . $id;

		$response = $this->vs->api_call($api_url, 'GET');

		if ($response['status'] == 200) {
			if (empty($id)) {
				return Auth_Vidoop_Category::from_categories_xml($response['body']);
			} else {
				return Auth_Vidoop_Category::from_xml($response['body']);
			}
		}
	}

	/**
	 * Get representative images for the specified category.
	 *
	 * @param string $id ID of category to get images for.
	 * @return array array of Auth_Vidoop_Category_Image objects
	 */
	public function get_category_images($id) {
		$api_url = $this->api_base . '/vs/categories/' . $id . '/representative_images';

		$response = $this->vs->api_call($api_url, 'GET');
		if ($response['status'] == 200) {
			return Auth_Vidoop_Category_Image::from_images_xml($response['body']);
		}
	}

}


/**
 * Category provided by the VidoopSecure Enrollment service.
 */
class Auth_Vidoop_Category {

	/** 
	 * Category ID
	 *
	 * @var string
	 */
	public $id;

	/**
	 * Canonical URI for this category.
	 *
	 * @var string
	 */
	public $uri;

	/**
	 * Category name.
	 *
	 * @var int
	 */
	public $name;

	/**
	 * Array of representative images for this category.
	 *
	 * @var array Array of Auth_Vidoop_Image objects
	 */
	public $images;

	/**
	 * Raw XML string this category object was created from.
	 *
	 * @var string
	 */
	public $xml;

	/**
	 * Generate a category instance from the provided XML.
	 *
	 * @param string $xml XML provided from VidoopSecure API
	 * @return Auth_Vidoop_Category category instance
	 */
	public static function from_xml($xml) {
		$simplexml = simplexml_load_string($xml);
		return self::from_simplexml($simplexml);
	}

	/**
	 * Generate a category instance from the provided SimpleXML object.
	 *
	 * @param SimpleXMLElement $simplexml xml object used to build category
	 * @return Auth_Vidoop_Category category instance
	 */
	public static function from_simplexml($simplexml) {
		$category = new Auth_Vidoop_Category();
		//$category->xml = $xml;
		$category->id = (string) $simplexml->id;
		$category->uri = (string) $simplexml['uri'];
		$category->name = (string) $simplexml->name;

		$category->images = array();
		foreach ($simplexml->representative_images->imageURI as $image) {
			$category->images[] = Auth_Vidoop_Category_Image::from_simplexml($image);
		}

		return $category;
	}

	/**
	 * Generate an array of categories from the provided XML.
	 *
	 * @param string $xml XML provided from VidoopSecure API
	 * @return array of Auth_Vidoop_Category objects
	 */
	public static function from_categories_xml($xml) {
		$simplexml = simplexml_load_string($xml);
		$categories = array();

		foreach ($simplexml->category as $category) {
			$categories[] = self::from_simplexml($category);
		}

		return $categories;
	}
}



/**
 * Image provided by the VidoopSecure Enrollment service.
 */
class Auth_Vidoop_Category_Image {

	/**
	 * URI of the image.
	 *
	 * @var string
	 */
	public $uri;

	/**
	 * Raw XML string this CAPTCHA object was created from.
	 *
	 * @var string
	 */
	public $xml;

	/**
	 * Generate an image instance from the provided XML.
	 *
	 * @param string $xml XML provided from VidoopSecure API
	 * @return Auth_Vidoop_Image image instance
	 */
	public static function from_xml($xml) {
		$simplexml = simplexml_load_string($xml);
		return self::from_simplexml($simplexml);
	}

	/**
	 * Generate a image instance from the provided SimpleXML object.
	 *
	 * @param SimpleXMLElement $simplexml xml object used to build category
	 * @return Auth_Vidoop_Category_Image image instance
	 */
	public static function from_simplexml($simplexml) {
		$image = new Auth_Vidoop_Category_Image();
		$image->uri = (string) $simplexml;

		return $image;
	}

	/**
	 * Generate an array of images from the provided XML.
	 *
	 * @param string $xml XML provided from VidoopSecure API
	 * @return array of Auth_Vidoop_Category_Image objects
	 */
	public static function from_images_xml($xml) {
		$simplexml = simplexml_load_string($xml);
		$images = array();

		foreach ($simplexml->imageURI as $image) {
			$images[] = self::from_simplexml($image);
		}

		return $images;
	}
}

?>
